matplotlib==2.2.3
numpy==1.15.1
opencv-python==3.4.2.17
scipy==1.1.0
seaborn==0.9.0
